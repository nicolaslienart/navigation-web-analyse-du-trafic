# Sélection des sites et pages

J'ai récupérer des sites parmi les plus consultés en France selon le [classement Alexa](https://www.alexa.com/topsites/countries/FR)
 * Google.fr
 * Orange.fr
 * LeBonCoin.fr
 * Cdiscount.com
 * LeMonde.fr
 * Wikipedia.org
 * MeteoFrance.com
 * LeFigaro.fr
 * Microsoft.com
 * JeuxVideo.com
 * Ameli.fr
 * SFR.fr
 * LaPoste.fr
 * Credit-Agricole.fr
 * Pole-Emploi.fr
 * Ouest-France.fr
 * Booking.com
 * Oui.sncf
 * Free.fr
 * CAF.fr
 * Ouigo.com

Puis j'ai sélectionné une à deux pages pour chaque domaine, avec soit la page d'accueil soit une page séléctionnée de manière arbitraire.
