console.log('browseTheWeb')

// NOTE dom.image-lazy-loading.enabled sur false
// Meilleure idée de la taille des pages, car certaines images sont chargées dynamiquement seulement quand elle sont visible à l'écran (ex : lors du scroll)

// NOTE browser.cache.disk.enable et browser.cache.memory.enable sur false
// Meilleure idée de la taille des pages, à cause de l'influence peu prédictible de la gestion du cache du navigateur


let pages = [
	// Wikipedia
	"https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal",
	"https://fr.wikipedia.org/wiki/Huile_d%27olive",
	// Orange
	"https://boutique.orange.fr/internet-mobile/pack-open-fibre/open-up-70go",
	"https://boutique.orange.fr/mobile/samsung-galaxy-a71-noir?segmentforfait=FORFAIT_ILLIMITE",
	// Le Bon Coin
	"https://www.leboncoin.fr/",
	"https://www.leboncoin.fr/chaussures/1822711774.htm/",
	// Cdiscount
	"https://www.cdiscount.com/soldes-promotions/v-14107-14107.html#cm_sp=PA:SL:4_1:SlideR%20Parasol%20/%20Cave",
	"https://www.cdiscount.com/bricolage/climatisation/whirlpool-pacb29co-2500-watts-climatiseur-mobile/f-1661301-whi8003437237669.html",
	// Le Monde
	"https://www.lemonde.fr/sciences/article/2020/07/30/la-nasa-a-lance-jeudi-son-astromobile-pour-chercher-des-traces-de-vie-passee-sur-mars_6047722_1650684.html",
	"https://www.lemonde.fr/planete/article/2020/08/19/coronavirus-dans-le-monde-la-course-au-vaccin-continue-l-oms-met-en-garde-contre-le-nationalisme-vaccinal_6049348_3244.html",
	// Météo France
	"http://meteofrance.com/",
	"http://meteofrance.com/meteo-marine",
	// Le Figaro
	"https://www.lefigaro.fr/",
	"https://www.lefigaro.fr/sciences/iena-ville-pionniere-du-port-du-masque-a-vu-le-virus-reculer-plus-vite-qu-ailleurs-20200818",
	// Microsoft
	"https://www.microsoft.com/fr-fr",
	"https://www.microsoft.com/fr-fr/microsoft-365?rtc=1",
	// Jeux Video
	"https://www.jeuxvideo.com/",
	"https://www.jeuxvideo.com/test/1269729/microsoft-flight-simulator-le-monde-comme-vous-ne-l-avez-jamais-vu.htm",
	// Ameli
	"https://www.ameli.fr/",
	"https://www.ameli.fr/assure/actualites",
	// SFR
	"https://www.sfr.fr/",
	"https://www.sfr.fr/reseau-5g/",
	// La Poste
	"https://www.labanquepostale.fr/",
	"https://www.laposte.fr/outils/suivre-vos-envois",
	// Crédit Agricole
	"https://www.credit-agricole.fr/",
	"https://www.credit-agricole.fr/particulier/credit.html",
	// Pole Emploi
	"https://www.pole-emploi.fr/accueil/",
	"https://www.pole-emploi.fr/candidat/mes-droits-aux-aides-et-allocati.html",
	// Ouest France
	"https://www.ouest-france.fr/",
	"https://www.ouest-france.fr/politique/gouvernement/oeuvres-d-art-en-cadeau-le-domicile-du-ministre-olivier-dussopt-perquisitionne-mardi-6942257",
	// Booking
	"https://www.booking.com/index.en-gb.html?label=gen173nr-1BCAEoggI46AdIM1gEaE2IAQGYAQm4ARnIAQ_YAQHoAQGIAgGoAgO4Aquk9fkFwAIB0gIkNzIxMWJkZjAtY2JlYS00NmZmLWE0YmQtNWExMjY3NGE5MzI42AIF4AIB;sid=c0e47eefd3fa331831c982ce7a9b1951;keep_landing=1&sb_price_type=total&",
	"https://www.booking.com/hotel/fr/radisson-sas-nice.en-gb.html?label=gen173nr-1FCAEoggI46AdIM1gEaE2IAQGYAQm4ARnIAQ_YAQHoAQH4AQuIAgGoAgO4Aquk9fkFwAIB0gIkNzIxMWJkZjAtY2JlYS00NmZmLWE0YmQtNWExMjY3NGE5MzI42AIG4AIB&sid=c0e47eefd3fa331831c982ce7a9b1951&dest_id=-1454990&dest_type=city&group_adults=2&group_children=0&hapos=1&hpos=1&no_rooms=1&sr_order=popularity&srepoch=1597854289&srpvid=6dfb7368ee1f0280&ucfs=1&from=searchresults;highlight_room=#hotelTmpl",
	// Oui SNCF
	"https://www.oui.sncf/",
	// Free
	"https://www.free.fr/freebox",
	"https://www.free.fr/freebox/freebox-mini-4k",
	// CAF
	"http://www.caf.fr/",
	"http://www.caf.fr/allocataires/actualites/2020/aide-au-logement-etudiant",
	// OUIGO
	"https://www.ouigo.com/"
]

let boucles
let index_link
let tabId

let url

const switchURL= (_, changeInfo) => {
	if (changeInfo.status === 'complete') {
		console.log('Chargement de la page terminé')
		switchUrlToNextLink()
		update()
	}
}

async function start() {
	console.log("Lancement de l'analyse", Date.now())
	boucles = 1
	index_link = 0
	url = pages[index_link]
	tabs = await browser.tabs.query({currentWindow: true})
	tabId = tabs.find(tab => tab.active === true).id

	browser.tabs.onUpdated.removeListener(switchURL)
	browser.tabs.onUpdated.addListener(switchURL, { tabId })

	update()
}

function switchUrlToNextLink() {
	index_link++

		if (index_link >= pages.length) {
			boucles--
			index_link = 0
		}

	url = pages[index_link]
}

function update() {
	console.log(`Changement d'URL : ${url}`)
	if (boucles > 0) {
		browser.tabs.update(tabId, {
			url
		}).then(() => {
		})
	}
	else {
		console.log("Analyse terminée", Date.now())
		browser.tabs.onUpdated.removeListener(switchURL)
		browser.tabs.update(tabId, {
			url: 'about:blank'
		})
	}
}

chrome.browserAction.onClicked.addListener(start)
