# Configurations

## Navigateur

Mozilla Firefox dans la version Developer Edition 80.0beta
Sorti le 28 juillet 2020

## Paramètres du navigateur

`about:preferences` > `Vie privée` > `Protection renforcée contre le pistage` :
Configuré en mode personnalisé avec certaines protections du navigateur décochées :
 - Cookies
 - Contenu utilisé pour le pistage

Le "Do not track" est aussi désactivé car il peut influencer les sites

## Extensions

Sans extension avec influence

## Configuration avancée `about:config`
`browser.cache.disk.enable`        désactivé
`browser.cache.memory.enable`      désactivé
`dom.image-lazy-loading.enabled`   désactivé

